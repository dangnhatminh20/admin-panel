import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { LayoutComponent } from './views/_layout/layout/layout.component';
import { ElementsComponent } from './views/elements/elements.component';
import { ChartsComponent } from './views/charts/charts.component';
import { PanelsComponent } from './views/panels/panels.component';
import { NotificationsComponent } from './views/notifications/notifications.component';
import { TablesComponent } from './views/tables/tables.component';
import { TypographyComponent } from './views/typography/typography.component';
import { IconsComponent } from './views/icons/icons.component';
import { PageLockscreenComponent } from './views/page-lockscreen/page-lockscreen.component';
import { PageLoginComponent } from './views/page-login/page-login.component';
import { PageProfileComponent } from './views/page-profile/page-profile.component';
import { AuthGuard } from './shared/auth/auth.guard';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'elements', component: ElementsComponent },
      { path: 'charts', component: ChartsComponent },
      { path: 'panels', component: PanelsComponent },
      { path: 'notifications', component: NotificationsComponent },
      { path: 'page-profile', component: PageProfileComponent },
      { path: 'tables', component: TablesComponent },
      { path: 'typography', component: TypographyComponent },
      { path: 'icons', component: IconsComponent }
    ],
    canActivate: [AuthGuard]
  },
  { path: 'page-login', component: PageLoginComponent },
  { path: 'page-lockscreen', component: PageLockscreenComponent }
  // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule

  ],
  declarations: [

  ],

  exports: [RouterModule]
})
export class AppRoutingModule { }

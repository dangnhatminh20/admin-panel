import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { HeaderComponent } from './views/_layout/header/header.component';
import { LayoutComponent } from './views/_layout/layout/layout.component';
import { SitebarComponent } from './views/_layout/sitebar/sitebar.component';
import { ElementsComponent } from './views/elements/elements.component';
import { ChartsComponent } from './views/charts/charts.component';
import { PanelsComponent } from './views/panels/panels.component';
import { NotificationsComponent } from './views/notifications/notifications.component';
import { TablesComponent } from './views/tables/tables.component';
import { TypographyComponent } from './views/typography/typography.component';
import { IconsComponent } from './views/icons/icons.component';
import { PageProfileComponent } from './views/page-profile/page-profile.component';
import { PageLoginComponent } from './views/page-login/page-login.component';
import { PageLockscreenComponent } from './views/page-lockscreen/page-lockscreen.component';
import { AuthGuard } from './shared/auth/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LayoutComponent,
    SitebarComponent,
    DashboardComponent,
    ElementsComponent,
    ChartsComponent,
    PanelsComponent,
    NotificationsComponent,
    PageProfileComponent,
    PageLoginComponent,
    PageLockscreenComponent,
    TablesComponent,
    TypographyComponent,
    IconsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }

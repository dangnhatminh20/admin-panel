import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private route: Router
    ) {}

    canActivate() {
        const checkLogin = JSON.parse(localStorage.getItem('isLogin'));
        if (checkLogin) {
            return true;
        }

        this.route.navigateByUrl('page-login');
        return false;
    }
}
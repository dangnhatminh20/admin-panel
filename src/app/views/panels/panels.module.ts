import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PanelsRoutingModule } from './panels-routing.module';
import { PanelsComponent } from './panels.component';


@NgModule({
  declarations: [PanelsComponent],
  imports: [
    CommonModule,
    PanelsRoutingModule
  ]
})
export class PanelsModule { }

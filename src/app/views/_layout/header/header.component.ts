import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  widthScreen: any;

  constructor() { 
    
  }

  ngOnInit() {
    this.onResize();
  }
  
  onLogout() {
    localStorage.clear();
  }

  onToggleNavbar(){
    const body = document.querySelector('body');
    const className = body.classList.contains('layout-fullwidth');

    if (!className) {
      body.classList.add('layout-fullwidth');
    } else {
      body.classList.remove('layout-fullwidth');
    }
    //icon
    document.querySelector('.lnr').classList.toggle('lnr-arrow-left-circle');
    document.querySelector('.lnr').classList.toggle('lnr-arrow-right-circle');
    //reponsive <1025px
    if(this.widthScreen < 1025) {
      if (!className) {
        body.classList.add('offcanvas-active');
      } else {
        body.classList.remove('offcanvas-active');
      }
    }
  }

  //Lấy kích cỡ của màn hình đang hiển thị
  @HostListener('window:resize', ['$event'])
  onResize($event?) {
    const width = window.innerWidth;
    this.widthScreen = width;
    console.log('width', width);
  }

}

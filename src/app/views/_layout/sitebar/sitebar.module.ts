import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SitebarRoutingModule } from './sitebar-routing.module';
import { SitebarComponent } from './sitebar.component';


@NgModule({
  declarations: [SitebarComponent],
  imports: [
    CommonModule,
    SitebarRoutingModule
  ]
})
export class SitebarModule { }

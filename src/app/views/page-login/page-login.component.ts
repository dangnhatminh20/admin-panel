import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.scss']
})
export class PageLoginComponent implements OnInit {

  constructor(private router: Router) { }
  onClickSubmit(value) {

    if(value.user == "nhatminh" && value.password == "123456") {
      //logout 
      //localStorage.clear();
      localStorage.setItem('isLogin', 'true');
      location.href = 'dashboard';
      // this.router.navigate(['dashboard']);
    }
    else {
      window.alert("Vui long nhap lai!")
    }
  }
  ngOnInit() {
    localStorage.setItem('isLogin', 'false');
  }

}

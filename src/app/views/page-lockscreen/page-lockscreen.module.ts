import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageLockscreenRoutingModule } from './page-lockscreen-routing.module';
import { PageLockscreenComponent } from './page-lockscreen.component';


@NgModule({
  declarations: [PageLockscreenComponent],
  imports: [
    CommonModule,
    PageLockscreenRoutingModule
  ]
})
export class PageLockscreenModule { }
